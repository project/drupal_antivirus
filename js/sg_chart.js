(function ($) {
    Drupal.behaviors.drupal_antivirus = {
        attach: function (context, settings) {

            var result = 0;

            var today = new Date();
            var d = today.getDate();
            var m = today.getMonth() + 1;
            var y = today.getFullYear();
            if (d < 10) {
                d = '0' + d;
            }
            if (m < 10) {
                m = '0' + m;
            }
            today = y + '-' + m + '-' + d;

            if (settings.drupal_antivirus.membership.trim().toLocaleLowerCase() == 'pro') {
                result += 30;
            }

            if (settings.drupal_antivirus.firewall_plan != 0 && settings.drupal_antivirus.plan_date > today) {
                var plan = settings.drupal_antivirus.firewall_plan.trim().toLocaleLowerCase();

                switch (plan) {
                    case "basic":
                        result += 30;
                        break;
                    case "standard":
                        result += 40;
                        break;
                    case "premium":
                    case "business":
                        result += 50;
                        break;
                }
            }

            if (settings.drupal_antivirus.backup == 1) {
                result += 20;
            }

            google.charts.load("current", {packages: ["corechart"]});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                    ['Option', 'Secured or no'],
                    ['Secured', result],
                    ['Issues', 100 - result]
                ]);

                var options = {
                    pieHole: 0.4,
                    slices: {
                        0: {color: '#16ab39'},
                        1: {color: '#db2828'}
                    },
                    chartArea: {left: '10%', top: '10%'},
                    legend: {textStyle: {fontSize: 16, bold: true}}
                };

                var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
                chart.draw(data, options);
            }


        }
    };

})(jQuery);