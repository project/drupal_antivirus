function form_confirm_registration(form) {

    if (!jQuery('#sg_term').is(':checked')){
        alert('Confirmation is not checked.');
        return false;
    }

    if (!is_email(jQuery('#sg_email').val())){
        alert('Email is not valid.');
        return false;
    }
    return true;
}

function is_email(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}