(function ($) {
    Drupal.behaviors.drupal_antivirus = {
        attach: function (context, settings) {

            $('#start_scan').click(function () {
                $('#start_scan').hide();
                $('#scanner_ajax_loader_avp').show();
                $('#scanner_ajax_reportbox').show();

                var refreshIntervalId;

                function get_report()
                {
                    $.ajax({
                        type: "POST",
                        url: settings.drupal_antivirus.sg_server_url,
                        data: {action:"getreport_ver2", data: settings.drupal_antivirus.report_data},
                        success: function(data){
                            console.log(data);
                            if (data != '') {
                                var result = JSON.parse(data);

                                if (result.status == 'ready') {

                                    clearInterval(refreshIntervalId);

                                    $('#start_scan').show();
                                    $('#latest_report').show();
                                    $('#scanner_ajax_loader_avp').hide();
                                    $('#scanner_ajax_reportbox').hide();
                                }
                            }
                        }
                    });
                }

                refreshIntervalId =  setInterval(get_report, 5000);

                $.post(settings.drupal_antivirus.base_url,
                    {
                        session_key: settings.drupal_antivirus.session_key
                    });
            })
        }
    };
})(jQuery);