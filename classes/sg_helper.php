<?php

require_once("sg_sql_helper.php");
require_once("sg_easyrequest.min.php");
require_once("sg_antivirus_helper.php");

class sg_helper
{

    public static  $antivirus_version = '1.0';

    public static  $antivirus_platform = 'drupal7';

    public static  $antivirus_cms = 'drupal7';

    static public $siteguarding_server = 'http://www.siteguarding.com/ext/antivirus/index.php';

    static public $blacklists_count = array();

    static public function scan($session_key){

        $params = self::get_license_info();

        $scan_path = dirname(__FILE__);

        $scan_path = str_replace(DIRECTORY_SEPARATOR.'sites'.DIRECTORY_SEPARATOR.'all'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'drupal_antivirus'.DIRECTORY_SEPARATOR.'classes', DIRECTORY_SEPARATOR, $scan_path);

        $tmp_folder = $scan_path . "webanalyze" . DIRECTORY_SEPARATOR;

        if(!file_exists($tmp_folder))
        {
            mkdir($tmp_folder, 0750, true);
        }

        $work_dir = str_replace(DIRECTORY_SEPARATOR.'classes',DIRECTORY_SEPARATOR,dirname(__FILE__));

        $scanner = new sg_antivirus_helper();

        $scanner->antivirus_version = self::$antivirus_version;
        $scanner->antivirus_platform = self::$antivirus_platform;
        $scanner->antivirus_cms = self::$antivirus_cms;

        $scanner->work_dir = $work_dir;
        $scanner->tmp_dir = $tmp_folder;
        $scanner->membership = trim($params['membership']);
        $scanner->scan_path = $scan_path;
        $scanner->access_key = trim($params['access_key']);
        $scanner->domain = trim($params['domain']);
        $scanner->email = trim($params['email']);
        $scanner->session_report_key = $session_key;

        $scanner->scanner();

    }

    static public function check_registration()
    {

        $db_data = sg_sql_helper::get_extra_params(array('registered'));

        if (count($db_data)) {
            return true;
        } else {
            return false;
        }
    }

    static public function send_registration($domain, $email)
    {

        $link = sg_helper::$siteguarding_server . '?action=register&type=json&data=';

        $access_key = md5(time() . rand());

        $data = array(
            'domain' => $domain,
            'email' => $email,
            'access_key' => $access_key,
            'cms' => 'any'
        );

        $link .= base64_encode(json_encode($data));

        $client = sg_easyrequest::create($link);
        $client->send();
        $msg = trim($client->getResponseBody());

        if ($msg === '') {
            $data = array(
                'registered' => 1,
                'access_key' => $access_key,
                'email' => $email,
                'domain' => $domain
            );

            sg_sql_helper::set_extra_params($data);

            $module_path = dirname(__FILE__);

            $root_path = str_replace(DIRECTORY_SEPARATOR.'sites'.DIRECTORY_SEPARATOR.'all'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'drupal_antivirus'.DIRECTORY_SEPARATOR.'classes', DIRECTORY_SEPARATOR, $module_path);

            if(!file_exists($root_path . "webanalyze")){
                mkdir($root_path . "webanalyze",0755);
                copy($module_path . DIRECTORY_SEPARATOR . 'antivirus.php', $root_path . "webanalyze" . DIRECTORY_SEPARATOR . 'antivirus.php' );
                $content = '<?php'."\n".
                    'define("ACCESS_KEY", "'.$access_key.'");'."\n".
                    '?>'."\n";
                $file = $root_path . 'webanalyze' . DIRECTORY_SEPARATOR . 'antivirus_config.php';
                self::create_file($file, $content);
            }

            return 'registered';
        } elseif (stripos($msg, 'is not exist') !== false) {
            return 'not_found';
        } elseif (stripos($msg, 'is already registered') !== false) {
            return 'already_registered';
        } else {
            return 'cant_communicate';
        }
    }

    static function create_file($file, $content)
    {
        if (file_exists($file)) unlink($file);
        $fp = fopen($file, 'w');
        $status = fwrite($fp, $content);
        fclose($fp);

        return $status;
    }

    static function get_license_info()
    {

        $cache_license_info = sg_sql_helper::get_extra_params(array('cache_license_info', 'cache_license_info_time'));

        if (isset($cache_license_info['cache_license_info']) && isset($cache_license_info['cache_license_info_time'])) {
            if (time() - (int)$cache_license_info['cache_license_info_time'] < 60 * 60) {
                $cache_license_info = (array)json_decode($cache_license_info['cache_license_info'], true);
                if (json_last_error() == JSON_ERROR_NONE) {
                    return $cache_license_info;
                }
            }
        }

        $link = sg_helper::$siteguarding_server . '?action=licenseinfo&type=json&data=';

        $link_https = sg_helper::$siteguarding_server . '?action=licenseinfo&type=json&data=';

        $website_info = sg_sql_helper::get_extra_params(array('domain', 'access_key'));

        $data = array(
            'domain' => $website_info['domain'],
            'access_key' => $website_info['access_key'],
            'product_type' => 'any'
        );

        $link .= base64_encode(json_encode($data));

        $link_https .= base64_encode(json_encode($data));

        $client = sg_easyrequest::create($link);
        $client->send();
        $response = trim($client->getResponseBody());

        if ($response == '') {
            $client = sg_easyrequest::create($link_https);
            $client->send();
            $response = trim($client->getResponseBody());
        }

        if ($response == '') {
            return false;
        }

        $cache_license_info = (array)json_decode($response, true);

        $data = array(
            'cache_license_info' => json_encode($cache_license_info),
            'cache_license_info_time' => time()
        );

        sg_sql_helper::set_extra_params($data);

        return $cache_license_info;

    }

    static public function avp_refresh_status(){
        $data = array('cache_license_info_time' => 0);
        sg_sql_helper::set_extra_params($data);
    }

    static public function check_firewall(){

        if(file_exists(DRUPAL_ROOT . DIRECTORY_SEPARATOR . 'webanalyze' . DIRECTORY_SEPARATOR . 'firewall' . DIRECTORY_SEPARATOR . 'firewall.php')) {
            return true;
        } else {
            return false;
        }
    }

    static public function get_admin_users() {

        $admin_data = array();

        $adminstrator_ids = array();

        $result_rid = db_query("SELECT rid FROM role  WHERE name LIKE :id",array(':id' => 'administrator'));

        $admin_rid = (int) $result_rid->fetchField(0);

        $result_uids = db_query("SELECT uid FROM users_roles WHERE rid='" . $admin_rid . "'")->fetchAll();

        if(count($result_uids)) {
            foreach ( $result_uids as $user ) {
                $adminstrator_ids[] = (int) $user->uid;
            }
        }

        foreach($adminstrator_ids as $id){
            $result_data = db_query("SELECT name,created,access,login,status FROM users WHERE uid=" . $id ."")->fetchAll();
            $admin_data[] = array($result_data[0]->name,$result_data[0]->created, $result_data[0]->access, $result_data[0]->login, $result_data[0]->status);
        }

        return $admin_data;
    }

    static public function get_blacklists_info()
    {

        $result_urlvoid = array();

        $api_urlvoid = array(
            '075d2746f96bc493d977e5c45c0e66457a147995',
            'd8a6c7bfc0bcdcafee9015f279fb87f0d2f98461',
            'e913bc7f9dd4c3d029774a8937ec0c6e48190ea2',
            'd99fdac6cbaed9d4549f1ba1b15f23950c7bcb54',
            'fcd3e995e2fd998bdaf63fa5c39423ec96fad48b',
            'b86d0094996fa5dedfa0a942d27081414ce4a9cb',
            '753b5c36de6bb9f7cfd726c7bf91020c1ecb547a',
            '095216e11be24a074ca4fe50a6d9bb8abd01e0c6',
            'dca6d53bf80cbd950cc6e2d4dce2d04772151342',
            'ed602b474bb3e1d670b5ed1ae43c8f323b736856',
            '2adc4c7b87647252fec79fde5a5ed2d01f7c57a7',
            'dbfee84de858035aafe6e26d81edd7c7b01660df',
            '91caa4eb6d2293099be5f3351c128cbdf957da9d',
            'fc2ed4f5a580c820c16302bf0348f33a2765758e'
        );

        $cache_blacklists_info = sg_sql_helper::get_extra_params(array('cache_blacklists_info', 'cache_blacklists_info_time'));

        if (isset($cache_blacklists_info['cache_blacklists_info']) && isset($cache_blacklists_info['cache_blacklists_info_time'])) {
            if (time() - (int)$cache_blacklists_info['cache_blacklists_info_time'] < 60 * 60) {
                $cache_blacklists_info = (array)json_decode($cache_blacklists_info['cache_blacklists_info'], true);
                if (json_last_error() == JSON_ERROR_NONE) {
                    return $cache_blacklists_info;
                }
            }
        }
        $website_info = sg_sql_helper::get_extra_params(array('domain'));

        $tmp_api_keys = $api_urlvoid;
        shuffle($tmp_api_keys);

        $link = "http://api.urlvoid.com/api1000/" . $tmp_api_keys[0] . "/host/" . sg_helper::prepare_domain($website_info['domain']) . "/rescan/";

        $client = sg_easyrequest::create($link);
        $client->send();
        $response = trim($client->getResponseBody());

        $output = json_decode(json_encode(simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOERROR | LIBXML_NOWARNING)), true);

        if ($output['action_result'] == 'ERROR') {
            $link = str_replace('/rescan/', '/scan/', $link);
            $client = sg_easyrequest::create($link);
            $client->send();
            $response = trim($client->getResponseBody());

            $output = json_decode(json_encode(simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOERROR | LIBXML_NOWARNING)), true);
        }

        if (isset($output['detections']['count']) && (int)$output['detections']['count'] > 0) {
            $result_urlvoid = $output['detections']['engines']['engine'];
        }

        $google_bl = sg_helper::scan_in_google($website_info['domain']);
        $mcafee_bl = sg_helper::scan_in_mcafee($website_info['domain']);
        $norton_bl = sg_helper::scan_in_norton($website_info['domain']);

        $result_blacklists = array($google_bl, $mcafee_bl, $norton_bl);

        $cache_blacklists_info = array_merge($result_blacklists, $result_urlvoid);

        $data = array(
            'cache_blacklists_info' => json_encode($cache_blacklists_info),
            'cache_blacklists_info_time' => time()
        );

        sg_sql_helper::set_extra_params($data);

        return $cache_blacklists_info;
    }

    static public function get_real_blacklists_info()
    {

        $blacklists = array(
            'Google' => array('logo' => 'http://www.google.com/s2/favicons?domain=google.com'),
            'McAfee' => array('logo' => 'http://www.google.com/s2/favicons?domain=mcafee.com'),
            'Norton' => array('logo' => 'http://www.google.com/s2/favicons?domain=norton.com'),
            'Quttera' => array('logo' => 'http://www.google.com/s2/favicons?domain=quttera.com'),
            'ZeroCERT' => array('logo' => 'http://www.google.com/s2/favicons?domain=zerocert.org'),
            'AVGThreatLabs' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.avgthreatlabs.com'),
            'Avira' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.avira.com'),
            'Bambenek Consulting' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.bambenekconsulting.com'),
            'BitDefender' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.bitdefender.com'),
            'CERT-GIB' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.cert-gib.com'),
            'CyberCrime' => array('logo' => 'http://www.google.com/s2/favicons?domain=cybercrime-tracker.net'),
            'c_APT_ure' => array('logo' => 'http://www.google.com/s2/favicons?domain=security-research.dyndns.org'),
            'Disconnect.me (Malw)' => array('logo' => 'http://www.google.com/s2/favicons?domain=disconnect.me'),
            'DNS-BH' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.malwaredomains.com'),
            'DrWeb' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.drweb.com'),
            'DShield' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.dshield.org'),
            'Fortinet' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.fortinet.com'),
            'GoogleSafeBrowsing' => array('logo' => 'http://www.google.com/s2/favicons?domain=developers.google.com'),
            'hpHosts' => array('logo' => 'http://www.google.com/s2/favicons?domain=hosts-file.net'),
            'Malc0de' => array('logo' => 'http://www.google.com/s2/favicons?domain=malc0de.com'),
            'MalwareDomainList' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.malwaredomainlist.com'),
            'MalwarePatrol' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.malware.com.br'),
            'MyWOT' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.mywot.com'),
            'OpenPhish' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.openphish.com'),
            'PhishTank' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.phishtank.com'),
            'Ransomware Tracker' => array('logo' => 'http://www.google.com/s2/favicons?domain=ransomwaretracker.abuse.ch'),
            'SCUMWARE' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.scumware.org'),
            'Spam404' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.spam404.com'),
            'SURBL' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.surbl.org'),
            'ThreatCrowd' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.threatcrowd.org'),
            'ThreatLog' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.threatlog.com'),
            'urlQuery' => array('logo' => 'http://www.google.com/s2/favicons?domain=urlquery.net'),
            'URLVir' => array('logo' => 'http://www.google.com/s2/favicons?domain=urlvir.com'),
            'VXVault' => array('logo' => 'http://www.google.com/s2/favicons?domain=vxvault.net'),
            'WebSecurityGuard' => array('logo' => 'http://www.google.com/s2/favicons?domain=www.websecurityguard.com'),
            'YandexSafeBrowsing' => array('logo' => 'http://www.google.com/s2/favicons?domain=yandex.com'),
            'ZeuS Tracker' => array('logo' => 'http://www.google.com/s2/favicons?domain=zeustracker.abuse.ch'),
        );

        $blacklist_array = array();
        $clean_array = array();

        $result_blacklists = sg_helper::get_blacklists_info();

        foreach ($blacklists as $k => $blacklist) {
            if (in_array($k, $result_blacklists)) {
                $blacklist['status'] = 1;
                $blacklist_array[$k] = $blacklist;
            } else {
                $blacklist['status'] = 0;
                $clean_array[$k] = $blacklist;
            }
        }

        sg_helper::$blacklists_count['blacklist'] = count($blacklist_array);
        sg_helper::$blacklists_count['clean'] = count($clean_array);

        return array_merge($blacklist_array, $clean_array);

    }

    static public function get_blacklists_timestamp()
    {

        $timestamp = sg_sql_helper::get_extra_params(array('cache_blacklists_info_time'));

        return date('Y-m-d H:i:s', $timestamp['cache_blacklists_info_time']);
    }

    static public function get_blackls_counter()
    {

        return sg_helper::$blacklists_count;

    }

    static public function blacklists_recheck_status()
    {
        $data = array('cache_blacklists_info_time' => 0);

        sg_sql_helper::set_extra_params($data);
    }

    static public function scan_in_google($domain)
    {

        $url = 'https://sb-ssl.google.com/safebrowsing/api/lookup?client=demo-app&key=AIzaSyBtFip7uxKIDAMCV9tQAfQZzFyW0_JQjuo&appver=1.5.2&pver=3.1&url=' . $domain;

        $client = sg_easyrequest::create($url);
        $client->send();
        $content = trim($client->getResponseBody());

        if ($content == '') {
            return '';
        } else {
            return 'Google';
        }
    }

    static public function scan_in_mcafee($domain)
    {
        $url = "http://www.siteadvisor.com/sites/" . sg_helper::prepare_domain($domain);

        $client = sg_easyrequest::create($url);
        $client->send();
        $content = trim($client->getResponseBody());

        if (strpos($content, 'siteYellow') === true || strpos($content, 'siteRed') === true) {
            return 'McAfee';
        } else {
            return '';
        }
    }

    static public function scan_in_norton($domain)
    {
        $url = "https://safeweb.norton.com/report/show?url=" . sg_helper::prepare_domain($domain);

        $client = sg_easyrequest::create($url);
        $client->send();
        $content = trim($client->getResponseBody());

        if (strpos($content, $domain) !== false) {
            if (strpos($content, 'SAFE') === false && strpos($content, 'UNTESTED') === false) {
                return 'Norton';
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    static public function prepare_domain($domain)
    {

        $domain = str_replace(array('http://', 'https://', '/'), '', $domain);

        if ($domain[0] == "w" && $domain[1] == "w" && $domain[2] == "w" && $domain[3] == ".") {
            $domain = str_replace("www.", "", $domain);
        }

        if (substr_count($domain, '.') > 1) {
            $pieces = explode(".", $domain);
            $last_piece = end($pieces);
            $domain = prev($pieces) . '.' . $last_piece;
            return $domain;
        }
        return $domain;
    }

}