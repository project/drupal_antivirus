<?php

include_once('sg_zip.php');

class sg_antivirus_helper {

    protected static $debug = true;
    public static $scanner_version = '1.0';

    protected static  $bool_list = array(0 => 'FALSE', 1 => 'TRUE');

    public static $SITEGUARDING_SERVER = 'http://www.siteguarding.com/ext/antivirus/index.php';

    public $antivirus_version = '';
    public $antivirus_platform = '';
    public $antivirus_cms = '';

    public $work_dir = '';
    public $tmp_dir = '';
    public $membership = '';
    public $scan_path = '';
    public $access_key = '';
    public $domain = '';
    public $email = '';
    public $session_report_key = '';

    public $exclude_folders_real = array();
    public $license_info = array();

    protected static $max_filesize = 1024000;

    public function AntivirusFinished()
    {
        if (self::$debug) {
            self::DebugLog('line');
        }

        $reason = error_get_last();
        if (self::$debug) {
            self::DebugLog(print_r($reason, true));
        }
        if (self::$debug) {
            self::DebugLog('PHP process has been terminated');
        }

        $fp = fopen($this->tmp_dir.'flag_terminated.tmp', 'w');
        $a = date("Y-m-d H:i:s")." Terminated";
        fwrite($fp, $a);
        fclose($fp);
    }

    public function AntivirusFileLock()
    {
        $lockFile = $this->tmp_dir.'scan.lock';

        $lockFp = fopen($lockFile, 'w');

        flock($lockFp, LOCK_UN);
        unlink($lockFile);
    }

    public function scanner($check_session = true, $show_results = true)
    {

        // Start scanning process
        error_reporting(0);
        ini_set('memory_limit', '256M');

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            define(DIRSEP, '\\');
        } else {
            define(DIRSEP, '/');
        }


        // Skip the 2nd scan process
        $lockFile = $this->tmp_dir.'scan.lock';

        if (file_exists($lockFile) && (time() - filemtime($lockFile)) < 60*5) {
            $error_msg = 'Another Scanning Process in the memory. Exit.';
            if (self::$debug) {
                self::DebugLog($error_msg);
            }
            return;
        }

        register_shutdown_function('self::AntivirusFileLock');

        $lockFp = fopen($lockFile, 'w');

        // Register any shutdown of the script
        register_shutdown_function('self::AntivirusFinished');



        $error_msg = 'Start Scan Process ver. '.$this->antivirus_version.' [scanner ver. '.self::$scanner_version.']';
        if (self::$debug) {
            self::DebugLog($error_msg, true);
        }



        // Load extra settings
        if (file_exists($this->work_dir.'settings.php')) {
            $error_msg = '=> Extra settings loaded';
            if (self::$debug) {
                self::DebugLog($error_msg);
            }

            require_once($this->work_dir.'settings.php');

            if (count($avp_settings)) {
                foreach ($avp_settings as $k => $v)
                {
                    $v_txt = $v;
                    if ($v === false) {
                        $v_txt = 'BOOL: false';
                    }
                    if ($v === true) {
                        $v_txt = 'BOOL: true';
                    }

                    $error_msg = 'Setting Value: '.strtoupper($k).' = '.$v_txt;
                    if (self::$debug) {
                        self::DebugLog($error_msg);
                    }

                    if (strtolower($v) == 'false') {
                        $v = false;
                    }
                    if (strtolower($v) == 'true') {
                        $v = true;
                    }
                    define(strtoupper($k), $v);
                }
            }
        }

        if ( trim($this->license_info['settings']) != '' ) {
            if (self::$debug) {
                self::DebugLog("Remote settings:\n".$this->license_info['settings']);
            }

            // Parse the settings
            $remote_settings = explode("\n", trim($this->license_info['settings']));
            if (count($remote_settings)) {
                foreach ($remote_settings as $v)
                {
                    $v = trim($v);
                    $v = explode(":", $v);
                    $v[0] = trim($v[0]);
                    $v[1] = trim($v[1]);
                    if (strtolower($v[1]) == 'false') {
                        $v[1] = false;
                    }
                    if (strtolower($v[1]) == 'true') {
                        $v[1] = true;
                    }
                    define(strtoupper($v[0]), $v[1]);
                }
            }
        }



        // Analyze of exclude folders
        if (file_exists($this->work_dir.'exclude_folders.php')) {
            $error_msg = '=> Exclude folders file loaded';
            if (self::$debug) {
                self::DebugLog($error_msg);
            }

            require_once($this->work_dir.'exclude_folders.php');
        }



        $tmp_result = set_time_limit ( 7200 );

        $error_msg = 'Change Time limit: '.self::$bool_list[intval($tmp_result)].' , Value: '.ini_get('max_execution_time');
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        $error_msg = 'Current Memory limit: '.ini_get('memory_limit');
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        $error_msg = 'OS info: '.PHP_OS.' ('.php_uname().')';
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        $error_msg = 'PHP ver: '.PHP_VERSION;
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        unlink($this->tmp_dir.'flag_terminated.tmp');
        unlink($this->tmp_dir.'filelist_'.md5($this->access_key).'.txt');

        // Remove old version logs
        if (file_exists($this->tmp_dir.'filelist.txt')) {
            unlink($this->tmp_dir.'filelist.txt');
        }
        if (file_exists($this->tmp_dir.'debug.log')) {
            unlink($this->tmp_dir.'debug.log');
        }
        if (file_exists($this->tmp_dir.'pack.zip')) {
            unlink($this->tmp_dir.'pack.zip');
        }
        if (file_exists($this->tmp_dir.'pack.tar')) {
            unlink($this->tmp_dir.'pack.tar');
        }
        if (file_exists($this->tmp_dir.'report_excluded_folders.php')) {
            unlink($this->tmp_dir.'report_excluded_folders.php');
        }
        if (file_exists($this->tmp_dir.'report_excluded_files.php')) {
            unlink($this->tmp_dir.'report_excluded_files.php');
        }
        if (file_exists($this->tmp_dir.'antivirus_error.log')) {
            unlink($this->tmp_dir.'antivirus_error.log');
        }




        // Some Init data
        $membership = $this->membership;
        $scan_path = $this->scan_path;
        $access_key = $this->access_key;
        $domain = $this->domain;
        $email = $this->email;
        $session_report_key = $this->session_report_key;

        // Some logs
        $error_msg = 'Domain: '.$domain;
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        $error_msg = 'Scan path: '.$scan_path;
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        $error_msg = 'Session report key: '.$session_report_key;
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        $error_msg = 'Report URL: https://www.siteguarding.com/antivirus/viewreport?report_id='.$session_report_key;
        if (self::$debug) self::DebugLog($error_msg);

        $error_msg = 'TMP folder: '.$this->tmp_dir;
        if (self::$debug) {
            self::DebugLog($error_msg);
        }


        if (file_exists($scan_path.'.htaccess.siteguarding')) {
            $error_msg = 'Found: .htaccess.siteguarding';
            if (self::$debug) {
                self::DebugLog($error_msg);

            }
            if (!rename( $scan_path.'.htaccess.siteguarding', $scan_path.'.htaccess')) {
                $error_msg = 'Cant rename: .htaccess.siteguarding -> .htaccess';
                if (self::$debug) {
                    self::DebugLog($error_msg);
                }
            }
        }

        /**
         * Analyze what way to use for packing
         */
        $ssh_flag = false;
        if ( function_exists('exec') ) {
            // Pack files with ssh
            $ssh_flag = true;
        }
        if (defined('SETTINGS_ONLY_ZIP') && SETTINGS_ONLY_ZIP) {
            $ssh_flag = false;
        }

        if (self::$debug) {
            self::DebugLog('line');
        }


        $files_list = array();
        if (defined('DEBUG_FILELIST') && DEBUG_FILELIST) {
            self::DebugFile($this->work_dir, true);
        }


        $error_msg = 'Collecting info about the files [METHOD 2]';
        if (self::$debug) {
            self::DebugLog($error_msg);
        }


        $exclude_folders_real = array();
        if (count($exclude_folders)) {
            foreach ($exclude_folders as $k => $ex_folder)
            {
                $ex_folder = $scan_path.trim($ex_folder);
                $exclude_folders_real[$k] = trim(str_replace(DIRSEP.DIRSEP, DIRSEP, $ex_folder));
            }
        }
        else $exclude_folders_real = array();

        if (defined('SCAN_ALL_SITES') && SCAN_ALL_SITES == 1) {
            // No action
        }
        else {
            // Exclude other sites from this scan
            $error_msg = 'Check for other websites';
            if (self::$debug) {
                self::DebugLog($error_msg);
            }

            $dir_array = array();
            if ($currentDir = opendir($scan_path)) {
                while ($file = readdir($currentDir))
                {
                    if (is_dir($scan_path.$file)) {
                        if ($file == '.' || $file == '..') {
                            continue;
                        }
                        $dir_array[] = $file;
                    }

                }
                closedir($currentDir);
            }
            sort($dir_array);

            $cms_dirs = array();

            foreach ($dir_array as $folder)
            {
                if ( file_exists( $scan_path.DIRSEP.$folder.DIRSEP."configuration.php" ) || file_exists( $scan_path.DIRSEP.$folder.DIRSEP."wp-config.php" ) ) {
                    $cms_dirs[] = DIRSEP.$folder.DIRSEP;
                }
            }

            if (count($cms_dirs)) {
                $error_msg = 'Found '.count($cms_dirs).' websites';
                if (self::$debug) {
                    self::DebugLog($error_msg);
                }

                foreach ($cms_dirs as $k => $ex_folder)
                {
                    $ex_folder = $scan_path.trim($ex_folder);
                    $ex_folder = trim(str_replace(DIRSEP.DIRSEP, DIRSEP, $ex_folder));
                    if (!in_array($ex_folder, $exclude_folders_real)) {
                        $exclude_folders_real[] = $ex_folder;
                    }
                }
            }
        }

        // Remote exclude folders
        if (defined('EXCLUDE_FOLDERS')) {
            $remote_excluded_folders = explode(",", EXCLUDE_FOLDERS);
            if ( count($remote_excluded_folders)) {
                foreach ($remote_excluded_folders as $k => $ex_folder)
                {
                    $ex_folder = $scan_path.trim($ex_folder);
                    $ex_folder = trim(str_replace(DIRSEP.DIRSEP, DIRSEP, $ex_folder));
                    if (!in_array($ex_folder, $exclude_folders_real)) {
                        $exclude_folders_real[] = $ex_folder;
                    }
                }
            }
        }

        // Remote include folders
        if (defined('INCLUDE_FOLDERS')) {
            $remote_included_folders = explode(",", INCLUDE_FOLDERS);
            if ( count($remote_included_folders)) {
                foreach ($remote_included_folders as $k => $in_folder)
                {
                    $in_folder = $scan_path.trim($in_folder);
                    $in_folder = trim(str_replace(DIRSEP.DIRSEP, DIRSEP, $in_folder));
                    if (in_array($in_folder, $exclude_folders_real)) {
                        unset( $exclude_folders_real[array_search($in_folder, $exclude_folders_real)] );
                    }
                }
            }
        }

        // Check if exluded folders are on the server
        if (count($exclude_folders_real)) {
            $exclude_folders_real_short = array();
            foreach($exclude_folders_real as $k => $v)
            {
                if (!file_exists($v)) {
                    unset($exclude_folders_real[$k]);
                }
                else $exclude_folders_real_short[] = str_replace($scan_path, DIRSEP, $v);
            }

            if (defined('HIDE_EXCLUDE_ALERT') && HIDE_EXCLUDE_ALERT == 1) {
                // Empty
            }
            else {
                $file_report_excluded_folders = $this->tmp_dir.'report_excluded_folders.php';

                $fp = fopen($file_report_excluded_folders, 'w');
                $status = fwrite($fp, "<?php die(); ?>\n".json_encode($exclude_folders_real_short));
                fclose($fp);
            }
        }


        $this->exclude_folders_real = $exclude_folders_real;


        $error_msg = 'Excluded Folders: '.count($exclude_folders_real);
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        $error_msg = print_r($exclude_folders_real, true);
        if (self::$debug && count($exclude_folders_real) > 0) {
            self::DebugLog($error_msg);
        }

        $dirList = array();
        $dirList[] = $scan_path;




        // Scan all dirs
        while (true)
        {
            $dirList = array_merge(self::ScanFolder(array_shift($dirList), $files_list), $dirList);
            if (count($dirList) < 1) {
                break;
            }
        }


        $error_msg = 'Save collected file_list';
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        $collected_filelist = $this->tmp_dir.'filelist_'.md5($this->access_key).'.txt';


        $fp = fopen($collected_filelist, 'w');
        $status = fwrite($fp, implode("\n", $files_list));
        fclose($fp);
        if ($status === false) {
            $error_msg = 'Cant save information about the collected files '.$collected_filelist;
            if (self::$debug) {
                self::DebugLog($error_msg);
            }

            // Turn ZIP mode
            $ssh_flag = false;
        }

        $error_msg = 'Total files: '.count($files_list);
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        if (self::$debug) {
            self::DebugLog('line');
        }




        if ($ssh_flag) {
            // SSH way
            $error_msg = 'Start - Pack with SSH';
            if (self::$debug) {
                self::DebugLog($error_msg);
            }

            $cmd = 'cd '.$scan_path.''."\n".'tar -czf '.$this->tmp_dir.'pack_'.md5($this->access_key).'.tar -T '.$collected_filelist;

            $output = array();
            $result = exec($cmd, $output);

            if (file_exists($this->tmp_dir.'pack_'.md5($this->access_key).'.tar') === false) {
                $ssh_flag = false;

                $error_msg = 'Change pack method from SSH to PHP (ZipArchive)';
                if (self::$debug) {
                    self::DebugLog($error_msg);
                }
            }
        }



        if (!$ssh_flag) {
            // PHP way
            $error_msg = 'Start - Pack with ZipArchive';
            if (self::$debug) {
                self::DebugLog($error_msg);
            }

            $file_zip = $this->tmp_dir.'pack_'.md5($this->access_key).'.zip';

            if (file_exists($file_zip)) {
                unlink($file_zip);
            }
            $pack_dir = $scan_path;


            if (class_exists('ZipArchive') && ( defined('DISABLE_ZIPARCHIVE') && DISABLE_ZIPARCHIVE === false ) ) {
                // open archive
                $zip = new ZipArchive;
                if ($zip->open($file_zip, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE) === TRUE) {
                    foreach ($files_list as $file_name)
                    {
                        $file_name = $this->scan_path.$file_name;
                        if( strstr(realpath($file_name), "stark") == FALSE) {
                            $short_key = str_replace($scan_path, "", $file_name);
                            $s = $zip->addFile(realpath($file_name), $short_key);
                            if (!$s) {
                                $error_msg = 'Couldnt add file: '.$file_name;
                                if (self::$debug) {
                                    self::DebugLog($error_msg);
                                }
                            }
                        }

                    }

                    $zip->close();

                }
                else {
                    $error_msg = 'Error: Couldnt open ZIP archive.';
                    if (self::$debug) {
                        self::DebugLog($error_msg);
                    }
                    return;
                }

            }
            else {
                $error_msg =  'Error: ZipArchive class is not exist.';
                if (self::$debug) {
                    self::DebugLog($error_msg);
                }
            }

            $error_msg = 'ZipArchive method - finished';
            if (self::$debug) {
                self::DebugLog($error_msg);
            }

            // Check if zip file exists
            if (!file_exists($file_zip)) {
                $error_msg = 'Error: zip file is not exists. Use OwnZipClass';
                if (self::$debug) {
                    self::DebugLog($error_msg);
                }

                $error_msg = 'OwnZipClass method - started';
                if (self::$debug) {
                    self::DebugLog($error_msg);
                }

                $zip = new Zip();
                $zip->setZipFile($file_zip);
                foreach ($files_list as $file_name_short)
                {
                    $file_name = trim($this->scan_path.$file_name_short);


                    if (defined('DEBUG_ZIP_ADDFILES') && DEBUG_ZIP_ADDFILES === true) {
                        $error_msg = 'Zip Add: '.$file_name;
                        if (self::$debug) {
                            self::DebugLog($error_msg);
                        }
                    }

                    $handle = fopen($file_name, "r");
                    if (filesize($file_name) > 0) {
                        $zip->addFile(fread($handle, filesize($file_name)), $file_name_short, filectime($file_name), NULL, TRUE, Zip::getFileExtAttr($file_name));
                    }
                    fclose($handle);
                }
                $zip->finalize();

                $error_msg = 'OwnZipClass method - finished';
                if (self::$debug) {
                    self::DebugLog($error_msg);
                }

                $ssh_flag = false;
            }

        }



        /**
         * Send files to SG server
         */
        if ($ssh_flag) {
            $archive_filename = $this->tmp_dir.'pack_'.md5($this->access_key).'.tar';
            $archive_format = 'tar';
        } else {
            $archive_filename = $this->tmp_dir.'pack_'.md5($this->access_key).'.zip';
            $archive_format = 'zip';
        }
        $error_msg = 'Pack file: '.$archive_filename;
        if (self::$debug) {
            self::DebugLog($error_msg);
        }



        // Check if pack file is exist
        if (file_exists($archive_filename) === false) {
            $error_msg = 'Error: Pack file is not exist. Probably not enough space on the server.';
            if (self::$debug) {
                self::DebugLog($error_msg);
            }
            return;
        }

        $tar_size = filesize($archive_filename);
        $error_msg = 'Pack file is '.round($tar_size/1024/1024, 2).'Mb';
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        if (self::$debug) {
            self::DebugLog('line');
        }

        $error_msg = 'Start - Send Packed files to SG server';
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        $archive_file_url = "/".str_replace($this->scan_path, "", $this->tmp_dir).'pack_'.md5($this->access_key).'.'.$archive_format;
        $archive_file_url = str_replace("\\", "/", $archive_file_url);
        $error_msg = 'Pack URL: '.$archive_file_url;
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        if ($tar_size < 32 * 1024 * 1024 || $membership == 'pro') {
            // Send file
            $post_data = base64_encode(json_encode(array(
                    'domain' => $domain,
                    'access_key' => $access_key,
                    'email' => $email,
                    'session_report_key' => $session_report_key,
                    'archive_format' => $archive_format,
                    'archive_file_url' => $archive_file_url))
            );

            $flag_CallBack = false;
            if (defined('CALLBACK_PACK_FILE') && CALLBACK_PACK_FILE ) {	// Callback option
                $flag_CallBack = true;
            }
            else {
                $result = self::UploadSingleFile($archive_filename, 'uploadfiles_ver2', $post_data);
                if ($result === false) {
                    $error_msg = 'Can not upload pack file for analyze';
                    if (self::$debug) {
                        self::DebugLog($error_msg);
                    }

                    $flag_CallBack = true;
                }
                else {
                    $error_msg = 'Pack file sent for analyze - OK';
                    if (self::$debug) {
                        self::DebugLog($error_msg);
                    }

                    $flag_CallBack = false;
                }
            }


            // CallBack method
            if ($flag_CallBack) {
                $error_msg = 'Start to use CallBack method';
                if (self::$debug) {
                    self::DebugLog($error_msg);
                }


                $post_data = base64_encode(json_encode(array(
                        'domain' => $domain,
                        'access_key' => $access_key,
                        'email' => $email,
                        'session_report_key' => $session_report_key,
                        'archive_format' => $archive_format,
                        'archive_file_url' => $archive_file_url))
                );

                $result = self::UploadSingleFile_Callback($post_data);

                if ($result === false) {
                    $error_msg = 'CallBack method - failed';
                    if (self::$debug) {
                        self::DebugLog($error_msg);
                    }

                    $error_msg = 'Can not upload pack file for analyze';
                    if (self::$debug) {
                        self::DebugLog($error_msg);
                    }
                    return;
                }
                else {
                    $error_msg = 'CallBack method - OK';
                    if (self::$debug) {
                        self::DebugLog($error_msg);
                    }
                }

            }


        }
        else {
            $error_msg = 'Pack file is too big ('.$error_msg.'), please contact SiteGuarding.com support or upgrade to PRO version.';
            if (self::$debug) {
                self::DebugLog($error_msg);
            }
            $error_msg = 'Website is huge or hosting account has other sites. Free version has limits. Contact SiteGuarding.com support.';
            self::ErrorLog($error_msg);
            return;
        }

        if (self::$debug) {
            self::DebugLog('line');

        }
        /**
         * Check and Get report from SG server
         */
        $error_msg = 'Start - Report generating';
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        $use_https_flag = false;
        for ($i = 1; $i <= 10*60; $i++)
        {
            sleep(5);

            /*$post_data = array(
                'data'=> base64_encode(json_encode(array(
                    'domain' => $domain,
                    'access_key' => $access_key,
                    'session_report_key' => $session_report_key)))
            );*/

            $post_data = base64_encode(json_encode(array(
                'domain' => $domain,
                'access_key' => $access_key,
                'session_report_key' => $session_report_key)));

            if ($use_https_flag === false){
                $link = self::$SITEGUARDING_SERVER.'?action=getreport_ver2&data='.$post_data;
                $result_json = file_get_contents($link);

                if ($result_json === false) {
                    $use_https_flag = true;
                }
            }

            if ($use_https_flag === true) {
                $link = str_replace("http://www.", "https://www.", self::$SITEGUARDING_SERVER).'?action=getreport_ver2&data='.$post_data;
                $result_json = file_get_contents($link);
                if ($result_json === false) {
                    $error_msg = 'Report can not be generated. Please try again or contact support';
                    if (self::$debug) {
                        self::DebugLog($error_msg);
                    }
                    return;
                }
            }


            $result_json = (array)json_decode($result_json, true);

            //if (self::$debug) self::DebugLog(print_r($result_json, true));

            if ($result_json['status'] == 'ready') {
                $error_msg = 'Done. Sending your report by email';
                if (self::$debug) {
                    self::DebugLog($error_msg);
                }
                return;
            }
        }



        $error_msg = 'Finished [Report is not sent]'."\n";
        if (self::$debug) {
            self::DebugLog($error_msg);
        }
    }

    function ScanFolder($path, &$files_list)
    {
        $dirList = array();

        if ($currentDir = opendir($path)) {
            while ($file = readdir($currentDir))
            {
                if ( $file === '.' || $file === '..' || is_link($path) ) {
                    continue;
                }
                $file = $path . '/' . $file;


                if (is_dir($file)) {
                    $folder = $file.DIRSEP;
                    $folder = str_replace(DIRSEP.DIRSEP, DIRSEP, $folder);

                    // Exclude folders
                    if (count($this->exclude_folders_real)) {
                        if (in_array($folder, $this->exclude_folders_real)) {
                            if (self::$debug) {
                                self::DebugLog('--- '.$folder);
                            }
                            continue;
                        }
                        else if (self::$debug) {
                            self::DebugLog('+++ '.$folder);
                        }
                    }
                    $dirList[] = $file;
                    if (defined('DEBUG_FILELIST') && DEBUG_FILELIST) {
                        self::DebugFile($file);
                    }
                }
                else {

                    if (is_link($file)) {
                        self::DebugLog('Symbolic link: '.$file);
                        continue;
                    }

                    if (strpos($file, '.php') === true || strpos($file, '.phtml') === true ) {
                        $file_size = filesize($file);
                        if ($file_size > self::$max_filesize) {
                            self::DebugLog('Big file: '.str_replace($this->scan_path, "", $file).' ['.$file_size.' bytes]');
                            self::HugeFilesLog(str_replace($this->scan_path, "", $file));
                            continue;
                        }

                        $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                        switch ($ext)
                        {
                            case 'tar':
                            case '7z':
                            case 'exe':
                            case 'gz':
                            case 'gzip':
                            case 'pak':
                            case 'pkg':
                            case 'rar':
                            case 'tar-gz':
                            case 'tgz':
                            case 'zip':
                                self::DebugLog('Excluded ext: '.$file);
                                continue;
                        }

                        if (strlen($this->scan_path) > 1) {
                            $file = str_replace($this->scan_path, "", $file);
                        }
                        if ($file[0] == "\\" || $file[0] == "/") {
                            $file[0] = "";
                        }
                        $file = trim($file);
                        $files_list[] = $file;
                    }
                    else {
                        // Check extension
                        $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                        switch ($ext)
                        {
                            case 'inc':
                            case 'php':
                            case 'php4':
                            case 'php5':
                            case 'phtml':
                            case 'js':
                            case 'html':
                            case 'htm':
                            case 'cgi':
                            case 'pl':
                            case 'so':
                            case 'sh':
                            case 'htaccess':

                                $file_size = filesize($file);
                                if ($file_size > self::$max_filesize) {
                                    self::DebugLog('Big file: '.str_replace($this->scan_path, "", $file).' ['.$file_size.' bytes]');
                                    self::HugeFilesLog(str_replace($this->scan_path, "", $file));
                                    continue;
                                }

                                if (strlen($this->scan_path) > 1) {
                                    $file = str_replace($this->scan_path, "", $file);
                                }
                                if ($file[0] == "\\" || $file[0] == "/") {
                                    $file[0] = "";
                                }
                                $file = trim($file);
                                $files_list[] = $file;
                                break;
                        }
                    }

                }

            }
            closedir($currentDir);

        }

        return $dirList;
    }

    public function DebugLog($txt, $clean_log_file = false)
    {
        if ($txt == 'line') {
            $txt = '-----------------------------------------------------------------------';
        }
        if ($clean_log_file) {
            $fp = fopen($this->tmp_dir.'debug_'.md5($this->access_key).'.log', 'w');
        }
        else $fp = fopen($this->tmp_dir.'debug_'.md5($this->access_key).'.log', 'a');
        $a = date("Y-m-d H:i:s")." ".$txt."\n";
        fwrite($fp, $a);
        fclose($fp);
    }

    function DebugFile($file, $clean_log_file = false)
    {
        if ($clean_log_file) {
            $fp = fopen($this->tmp_dir.'debug_filelist.log', 'w');
        }
        else {
            $file = str_replace($this->scan_path, "", $file);
            $fp = fopen($this->tmp_dir.'debug_filelist.log', 'a');
        }

        $a = $file."\n";
        fwrite($fp, $a);
        fclose($fp);
    }

    function UploadSingleFile($file, $action, $post_data)
    {
        $status = self::UploadSingleFile_extension($file, $action, $post_data);
        if ($status === false) {
            $status = self::UploadSingleFile_extension($file, $action, $post_data, true);
        }
        return $status;
    }

    function UploadSingleFile_extension($file, $action, $post_data, $https_flag = false)
    {
        $target_url = self::$SITEGUARDING_SERVER;
        if ($https_flag === true) {
            $target_url = str_replace("http://www.", "https://www.", $target_url);
        }
        $target_url = $target_url.'?action='.$action;

        $file_name_with_full_path = $file;

        if (class_exists('CurlFile')) {
            $error_msg = 'CURLOPT: CurlFile';
            if (self::$debug) {
                self::DebugLog($error_msg);
            }

            $post = array(
                'data' => $post_data,
                'file_contents' => new \CurlFile($file_name_with_full_path)
            );
        }
        else {
            $error_msg = 'CURLOPT: @file';
            if (self::$debug) {
                self::DebugLog($error_msg);
            }

            $post = array(
                'data' => $post_data,
                'file_contents' => '@'.$file_name_with_full_path
            );
        }


        $error_msg = 'CURL CURLOPT_INFILE: '.$file_name_with_full_path;
        if (self::$debug) {
            self::DebugLog($error_msg);
        }
        $error_msg = 'CURL CURLOPT_INFILESIZE: '.filesize($file_name_with_full_path);
        if (self::$debug) {
            self::DebugLog($error_msg);
        }
        $error_msg = 'Use HTTPS: '.self::$bool_list[intval($https_flag)];
        if (self::$debug) {
            self::DebugLog($error_msg);
        }


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$target_url);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_SAFE_UPLOAD,false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_INFILE, $file_name_with_full_path);
        curl_setopt($ch, CURLOPT_INFILESIZE, filesize($file_name_with_full_path));
        $result=curl_exec ($ch);

        $info = curl_getinfo($ch);
        $error_msg = 'CURL info - '.print_r($info, true);
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        $curl_error = curl_error($ch);
        curl_close ($ch);

        if ($info['size_upload'] < 10000/*filesize($file_name_with_full_path)*/) {
            $error_msg = 'CURL uploaded file wrong size: '.$info['size_upload'];
            if (self::$debug) {
                self::DebugLog($error_msg);
            }

            return false;
        }

        if (!$result) {
            $error_msg = 'CURL upload is failed - '.$curl_error;
            if (self::$debug) {
                self::DebugLog($error_msg);
            }

            return false;
        }
        else return true;
    }

    function UploadSingleFile_Callback($post_data)
    {
        $status = $this->UploadSingleFile_Callback_extension($post_data);
        if ($status === false) {
            $status = $this->UploadSingleFile_Callback_extension($post_data, true);
        }

        return $status;
    }

    function UploadSingleFile_Callback_extension($post_data, $https_flag = false)
    {
        $error_msg = 'Use HTTPS: '.self::$bool_list[intval($https_flag)];
        if (self::$debug) {
            self::DebugLog($error_msg);
        }

        $link = self::$SITEGUARDING_SERVER;
        if ($https_flag === true) {
            $link = str_replace("http://www.", "https://www.", $link);
        }
        $link = $link.'?action=uploadfiles_callback';

        $postdata = http_build_query(
            array(
                'data' => $post_data
            )
        );

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );

        $context  = stream_context_create($opts);

        $result = file_get_contents($link, false, $context);

        $result = json_decode(trim($result), true);

        if (self::$debug) self::DebugLog(print_r($result, true));

        if ($result['status'] == 'ok') return true;
        else return false;
    }

    public function HugeFilesLog($txt)
    {
        $file = $this->tmp_dir.'report_excluded_files.php';
        if (!file_exists($file)) {
            $txt = "<?php die(); ?>\n".$txt;
        }
        $fp = fopen($file, 'a');
        fwrite($fp, $txt."\n");
        fclose($fp);
    }

    public function ErrorLog($txt)
    {
        $fp = fopen($this->tmp_dir.'antivirus_error.log', 'w');
        $a = date("Y-m-d H:i:s")." ".$txt."\n";
        fwrite($fp, $a);
        fclose($fp);
    }

}