<?php

class sg_sql_helper {
    
    static public $table_name = 'sg_antivirus';
    
    static public function get_extra_params(array $var_name) {
        
        $table = db_query("SHOW TABLES LIKE '" . sg_sql_helper::$table_name ."'")->fetchField();
        
        if($table != sg_sql_helper::$table_name) {
            return false;
        }
        
        $result = array();
        
        if(count($var_name) > 0 ) {
            foreach ($var_name as $k => $v) 
            {
                $var_name[$k] = "'".$v."'";
            }
            $query_string = "WHERE var_name IN (".implode(",", $var_name).")";
        } else {
            $query_string = "";
        }
        
        $rows = db_query("SELECT * FROM " . sg_sql_helper::$table_name . " " . $query_string)->fetchAll();
        
         if(count($rows)) {
            foreach ( $rows as $row ) {
                $result[trim($row->var_name)] = trim($row->var_value);
            }
        }
        
        return $result;
    }
    
    static public function set_extra_params(array $data) {
        
        if (count($data) == 0) {
            return false;
        }
        
        foreach ($data as $k => $v) {
            
            $tmp_res = db_query('SELECT * FROM ' . sg_sql_helper::$table_name . ' WHERE var_name = \'' . $k . '\' LIMIT 1;')->fetchAll();
                     
            if (count($tmp_res)) {
                db_update(sg_sql_helper::$table_name)->fields(array('var_name' => $k, 'var_value' => $v))->condition('var_name', $k, '=')->execute();
            } else {
                db_insert(sg_sql_helper::$table_name)->fields(array('var_name' => $k, 'var_value' => $v))->execute();
            }
        }
    }
}