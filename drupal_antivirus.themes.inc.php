<?php

function template_preprocess_antivirus_scanner(&$vars)
{

    $data = sg_helper::get_license_info();

    $vars['session_key'] = md5($data['domain'].time().mt_rand());

    $vars['sg_server_url'] = sg_helper::$siteguarding_server;

    $vars['report_data'] = base64_encode(json_encode(array(
        'domain' => $data['domain'],
        'access_key' => $data['access_key'],
        'session_report_key' => $vars['session_key']
    )));

    $vars['membership'] = strtolower(trim($data['membership']));

    $vars['filemonitoring_status'] = (isset($data['filemonitoring']['status'])) ? strtolower(trim($data['filemonitoring']['status'])) : null;

    $vars['footer'] = theme('drupal_antivirus_footer_block_template');

    $vars['reports'] = $data['reports'];

    $vars['last_scan_files_counters_heuristic'] = (isset($data['last_scan_files']['heuristic'])) ? $data['last_scan_files']['heuristic'] : null;

    $vars['filemonitoring_plan'] = (isset($data['filemonitoring']['plan'])) ? strtolower(trim($data['filemonitoring']['plan'])) : null;

    drupal_add_js(array('drupal_antivirus' => array('sg_server_url' => $vars['sg_server_url'], 'report_data' => $vars['report_data'], 'base_url' => $GLOBALS['base_url'] . "/admin/antivirus/scanner", 'session_key' => $vars['session_key'])), array('type' => 'setting'));

    drupal_add_js(drupal_get_path('module', 'drupal_antivirus') . '/js/sg_antivirus_scanner.js');
}

function template_preprocess_registration()
{
    drupal_add_css(drupal_get_path("module", "drupal_antivirus") . "/css/sg_antivirus_registration.css");
    drupal_add_js(drupal_get_path('module', 'drupal_antivirus') . '/js/sg_antivirus_registration.js');
}

function template_preprocess_help(&$vars)
{
    $vars['footer'] = theme('drupal_antivirus_footer_block_template');
}

function template_preprocess_dashboard(&$vars)
{

    drupal_add_js(drupal_get_path('module', 'drupal_antivirus') . '/js/sg_loader.js');

    $data = sg_helper::get_license_info();

    $vars['firewall_status'] = sg_helper::check_firewall();

    $vars['membership'] = strtolower(trim($data['membership']));

    $vars['exp_date'] = strtolower(trim($data['exp_date']));

    $vars['scans'] = strtolower(trim($data['scans']));

    $vars['reports'] = $data['reports'];

    $vars['admin_users'] = sg_helper::get_admin_users();

    $vars['filemonitoring_status'] = $data['filemonitoring']['status'];

    $vars['last_scan_files_counters_main'] = (isset($data['last_scan_files_counters']['main'])) ? $data['last_scan_files_counters']['main'] : null;

    $vars['last_scan_files_counters_heuristic'] = (isset($data['last_scan_files_counters']['heuristic'])) ? $data['last_scan_files_counters']['heuristic'] : null;

    $vars['filemonitoring_plan'] = (isset($data['filemonitoring']['plan'])) ? strtolower(trim($data['filemonitoring']['plan'])) : null;

    $vars['filemonitoring_exp_date'] = (isset($data['filemonitoring']['exp_date'])) ? strtolower(trim($data['filemonitoring']['exp_date'])) : null;

    $vars['remote_backup_status'] = (isset($data['filemonitoring']['remote_backup_status'])) ? strtolower(trim($data['filemonitoring']['remote_backup_status'])) : null;

    $vars['footer'] = theme('drupal_antivirus_footer_block_template');

    drupal_add_js(array('drupal_antivirus' => array('membership' => $vars['membership'], 'firewall_plan' => $vars['filemonitoring_plan'], 'backup' => $vars['remote_backup_status'], 'plan_date' => $vars['filemonitoring_exp_date'])), array('type' => 'setting'));

    drupal_add_js(drupal_get_path('module', 'drupal_antivirus') . '/js/sg_chart.js');

}

function template_preprocess_blacklists(&$vars)
{
    $vars['footer'] = theme('drupal_antivirus_footer_block_template');
    $data = sg_helper::get_license_info();
    $vars['membership'] = (strtolower(trim($data['membership'])) == 'pro') ? true : false;
    $vars['filemonitoring'] = (isset($data['filemonitoring']['status']) && $data['filemonitoring']['status'] > 0) ? true : false;
    $vars['blacklists'] = sg_helper::get_real_blacklists_info();
    $vars['blacklists_timestamp'] = sg_helper::get_blacklists_timestamp();
    $vars['blacklists_count'] = sg_helper::get_blackls_counter();

}

function template_preprocess_settings_tools(&$vars)
{

    $vars['footer'] = theme('drupal_antivirus_footer_block_template');

    $settings = sg_sql_helper::get_extra_params(array('registered', 'protected_by', 'access_key'));

    $data = sg_helper::get_license_info();

    $vars['access_key'] = (isset($settings['access_key']) && !empty($settings['access_key'])) ? $settings['access_key'] : '';

    $vars['registered'] = (!empty($settings['registered']) && $settings['registered'] == 1) ? '' : 'disabled';

    $vars['protected_by'] = (!empty($settings['protected_by']) && $settings['protected_by'] == 'on') ? 'checked' : '';

    $vars['membership'] = ($data['membership'] != 'pro' && $vars['registered'] == '') ? 'disabled' : '';

}

