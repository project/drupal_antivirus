<h1 class="ui dividing header">
    Blacklist status
</h1>
<div class="ui grid">
    <div class="row">
        <div class="ui two steps">
            <div class="step">
                <i class="<?php if ($membership) {
                    echo "check green";
                } else {
                    echo "times red";
                } ?> icon"></i>
                <div class="content">
                    <div class="title">Blacklist Scanner</div>
                    <div class="description">You can check your website in 30+ blacklists</div>
                </div>
            </div>
            <div class="step">
                <i class="<?php if ($filemonitoring) {
                    echo "check green";
                } else {
                    echo "times red";
                } ?> icon"></i>
                <div class="content">
                    <div class="title">Blacklist Removal & Monitoring</div>
                    <div class="description">If any issue detected, we will fix your website</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="ten wide column">
            <?php foreach ($blacklists as $k => $data) {
                if ($data['status'] == 0) { ?>
                    <div class="ui green icon large message">
                        <i class="check square outline icon"></i>

                        <div class="content">
                            Not blacklisted in <img src="<?php echo $data['logo']; ?>">
                            <b><?php echo $k; ?></b>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="ui red icon large message">
                        <i class="exclamation triangle icon"></i>

                        <div class="content">
                            Blacklisted in <img src="<?php echo $data['logo']; ?>">
                            <b><?php echo $k; ?></b>
                        </div>
                    </div>
                <?php }
            } ?>
        </div>
        <div class="six wide column">
            <div class="ui raised segment">
                <h3 class="ui dividing header">
                    Blacklist Status
                </h3>
                <div>
                    <b>Latest check: </b> <?php echo $blacklists_timestamp; ?>
                </div>
                <div>
                    <b>Blacklisted: </b> <?php echo $blacklists_count['blacklist']; ?>
                </div>
                <div>
                    <b>Clean: </b> <?php echo $blacklists_count['clean']; ?>
                </div>
                <div class="standard_alignment">
                    <form action="/admin/antivirus/blacklists" method="post" class="ui form">
                        <input name="recheck_blacklists" type="hidden" value="start">
                        <button class="medium positive ui button" type="submit">
                            Recheck
                        </button>
                        <br>
                        It can take some time
                    </form>
                </div>
                <p>
                    <i class="user md green icon"></i>Premium customers can request free cleaning.
                    Please contact
                    <a href="https://www.siteguarding.com/en/contacts" target="_blank">SiteGuarding.com
                        support</a>
                </p>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>