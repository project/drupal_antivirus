<div class="ui icon green message">
    <i class="medkit icon"></i>

    <div class="content">
        <h2 class="ui header standard_alignment">Got hacked or blacklisted? We can help.</h2>

        <p>Over 450.000 clients use our services daily. Based on extensive experience focused in Information
            Security we can assure you the best service and the best prices in the Globe. Before your
            business is a victim by intruders and think of your Business reputation damage. Consider to
            outsource your overall website security to professionals. Even if you have been a victim, we can
            take care of all the needed processes to bring back everything in place such as Google's
            reputation loss recovery, email delay processes, etc. Our Research & Development experts scan
            daily thousands of attacks and update all firewalls and customers on the fly.</p>

        <p class="standard_alignment">
            <a class="medium positive ui button" href="https://www.siteguarding.com/en/protect-your-website"
               target="_blank">Upgrade to PREMIUM</a>
            <a class="medium negative ui button"
               href="https://www.siteguarding.com/en/services/malware-removal-service" target="_blank">Clean
                Website</a>
        </p>
    </div>
</div>