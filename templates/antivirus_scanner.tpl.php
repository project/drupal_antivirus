<h1 class="ui dividing header">
    Antivirus scanner
</h1>
<div class="ui grid">
    <div class="row">
        <div class="ui three steps">
            <div class="step">
                <i class="<?php if ($membership == 'pro') {
                    echo "check green";
                } else {
                    echo "times red";
                } ?> icon"></i>

                <div class="content">
                    <div class="title">Antivirus scanner</div>
                    <div class="description">detect malware codes on your website</div>
                </div>
            </div>
            <div class="step">
                <div class="step">
                    <i class="<?php if ($filemonitoring_status > 0) {
                        echo "check green";
                    } else {
                        echo "times red";
                    } ?> icon"></i>

                    <div class="content">
                        <div class="title">File Analysis</div>
                        <div class="description">manual analyze of detected file</div>
                    </div>
                </div>
            </div>
            <div class="step">
                <div class="step">
                    <div class="step">
                        <i class="<?php if ($filemonitoring_status > 0 && $filemonitoring_plan != 'basic') {
                            echo "check green";
                        } else {
                            echo "times red";
                        } ?> icon"></i>

                        <div class="content">
                            <div class="title">File Analysis</div>
                            <div class="description">manual analyze of detected file</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="ten wide column">
            <div class="ui blue message">To start the scan process click "Start Scanner" button.</div>
            <p>Scanner will automatically collect and analyze the files of your website. The scanning
                process can take up to 10 minutes (it depends of speed of your server and amount of the
                files to analyze). The copy of the report we will send by email for your
                records.</p> </br>
            <div class="standard_alignment">
                <button id="start_scan" class="big positive ui button" type="submit">
                    Start Scanner
                </button>
                </br>
                </br>
                Scanner will check all the files for this website and all the folders.</br>
                <span class="c_red"><b>Please
                                        note:</b> Folders with other sites will be excluded</span></br>
                <img id="scanner_ajax_loader_avp"
                     src="<?php print "/" . drupal_get_path('module','drupal_antivirus') . "/images/ajax_loader.svg"; ?>"/>

                <div id="scanner_ajax_reportbox" class="ui small blue message"> Please note full scan
                    process can take upto 5-10 minutes. If it takes too long please contact
                    SiteGuarding.com support or use this link to see the current status.<br/><br/><b><a
                                href="<?php echo 'https://www.siteguarding.com/antivirus/viewreport?report_id=' . $session_key . '&refresh=1'; ?>"
                                target="_blank"><?php echo 'https://www.siteguarding.com/antivirus/viewreport?report_id=' . $session_key . '&refresh=1'; ?></a></b>
                </div>
                <div id="latest_report"><a class="positive ui button"
                                           href="<?php echo 'https://www.siteguarding.com/antivirus/viewreport?report_id=' . $session_key . '&refresh=1'; ?>"
                                           target="_blank">Click to see latest report</a></div>
            </div>

            <div class="ui info icon message">
                <i class="info letter icon"></i>

                <div class="content">
                    <div class="header">Free Professional Consultation</div>
                    We will be more than happy to provide free professional consultation with detailed
                    explanations of all the issues on your website.
                </div>
            </div>
            <div class="ui raised segment">
                <h3 class="ui dividing header">Detected Files</h3>
                <?php if (isset($reports) && count($reports) == 0) { ?>
                    <p>Please scan your website.</p>
                <?php } elseif (isset($last_scan_files_counters_heuristic) && count($last_scan_files_counters_heuristic) > 0) { ?>
                    <table class="ui celled table">
                        <thead>
                        <tr>
                            <th>File</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($last_scan_files_counters_heuristic as $row) { ?>
                            <tr>
                                <td>
                                    <?php echo $row; ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <p class="c_red">Heuristic algorithm has the capability of detecting malware that
                        was previously unknown. It doesn't give 100% guarantee that the file is the
                        virus and requires manual review. If these files are not a part of plugins,
                        extentions or website, delete or block them. <b>If you are not sure, you always
                            can contact our support and we will analyze the files.</b> 95% these files
                        have malicious code/scripts or contain code elements and commands those have
                        been used in different malicious scripts. Review is required.</p>
                <?php } else { ?>
                    <p>Problems with the files are not detected.</p>
                <?php } ?>
            </div>
        </div>
        <div class="six wide column">
            <div class="ui raised segment">
                <h3 class="ui dividing header">
                    Latest Antivirus Reports
                </h3>
                <div class="ui list">
                    <?php $sum_reports = count($reports);
                    if ($sum_reports > 0) {
                        for ($i = 0; $i < $sum_reports; $i++) { ?>
                            <div class="item">
                                <i class="file alternate outline icon"></i>
                                <div class="content"><a
                                            href="<?php echo $reports[$i]['report_link']; ?>"
                                            target="_blank">Antivirus
                                        report <?php echo $reports[$i]['date']; ?></a></div>
                            </div>
                            <?php if ($i == 4) {
                                break;
                            }
                        }
                    } else { ?>
                        You don't have any reports. Go to Antivirus scanner tab and scan the website.
                    <?php } ?>
                </div>
                <div class="standard_alignment">
                    <b> Need help from professional web security expert? </b><br><br>
                    <a class="ui medium negative button" target="_blank"
                       href="https://www.siteguarding.com/en/services/malware-removal-service">Clean
                        Website</a>
                </div>
                <p>
                    <i class="user md green icon"></i>Premium customers can request free cleaning.
                    Please contact
                    <a href="https://www.siteguarding.com/en/contacts" target="_blank">SiteGuarding.com
                        support</a>
                </p>
            </div>
        </div>
    </div>
</div>