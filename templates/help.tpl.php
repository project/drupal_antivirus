<h1 class="ui dividing header">
    Help & Support
</h1>
<div class="ui two column grid">
    <div class="column">
        <div class="ui segment">
            <h3 class="ui centered header">Free Support</h3>
            <div class="ui list">
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Installation and configuration of antivirus
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Help to check and analyze your website
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Support by email or live chat
                    </div>
                </div>
                <div class="item">
                    <i class="times red icon"></i>

                    <div class="content">
                        Daily update of antivirus database
                    </div>
                </div>
                <div class="item">
                    <i class="times red icon"></i>

                    <div class="content">
                        Full and detailed antivirus report
                    </div>
                </div>
                <div class="item">
                    <i class="times red icon"></i>

                    <div class="content">
                        Malware Removal (Hack repair and restoration, Trojan detection, Vulnerability
                        repair, SEO poisoining recovery)
                    </div>
                </div>
                <div class="item">
                    <i class="times red icon"></i>

                    <div class="content">
                        Help to remove your website from blacklists (Google, McAfee, Norton and etc.)
                    </div>
                </div>
                <div class="item">
                    <i class="times red icon"></i>

                    <div class="content">
                        Website 24/7 monitoring service
                    </div>
                </div>
                <div class="item">
                    <i class="times red icon"></i>

                    <div class="content">
                        Website full backup service and restoring
                    </div>
                </div>
                <div class="item">
                    <i class="times red icon"></i>

                    <div class="content">
                        Website scripts/codes analyze and bugs fix to avoid possibility for future hacks
                    </div>
                </div>
                <div class="item">
                    <i class="times red icon"></i>

                    <div class="content">
                        Premium firewall rules (help to configure firewall and apply more suitable
                        firewall rules for your website)
                    </div>
                </div>
                <div class="item">
                    <i class="times red icon"></i>

                    <div class="content">
                        Server log analyze & Issue investigation
                    </div>
                </div>
                <div class="item">
                    <i class="times red icon"></i>

                    <div class="content">
                        SSL certificate (https:// green lock in address bar)
                    </div>
                </div>
                <div class="item">
                    <i class="times red icon"></i>

                    <div class="content">
                        Unlock all security extensions
                    </div>
                </div>
                <div class="item">
                    <i class="times red icon"></i>

                    <div class="content">
                        High priority support
                    </div>
                </div>
                <div class="standard_alignment"><a class="ui medium button"
                                                   href="https://www.siteguarding.com/en/contacts"
                                                   target="_blank">Contact Support</a></div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="ui segment">
            <h3 class="ui centered header">Premium Support</h3>
            <div class="ui list">
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Installation and configuration of antivirus
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Help to check and analyze your website
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Support by email or live chat
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Daily update of antivirus database
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Full and detailed antivirus report
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Malware Removal (Hack repair and restoration, Trojan detection, Vulnerability
                        repair, SEO poisoining recovery)
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Help to remove your website from blacklists (Google, McAfee, Norton and etc.)
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Website 24/7 monitoring service
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Website full backup service and restoring
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Website scripts/codes analyze and bugs fix to avoid possibility for future hacks
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Premium firewall rules (help to configure firewall and apply more suitable
                        firewall rules for your website)
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Server log analyze & Issue investigation
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        SSL certificate (https:// green lock in address bar)
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        Unlock all security extensions
                    </div>
                </div>
                <div class="item">
                    <i class="check green icon"></i>

                    <div class="content">
                        High priority support
                    </div>
                </div>
            </div>
            <div class="standard_alignment"><a class="ui medium positive button"
                                               href="https://www.siteguarding.com/en/buy-service/antivirus-complete-website-protection"
                                               target="_blank">Upgrade to Premium</a>&nbsp;<a
                    class="ui medium positive button" href="https://www.siteguarding.com/en/contacts"
                    target="_blank">Contact Support</a></div>
        </div>
    </div>
</div>
<?php echo $footer; ?>