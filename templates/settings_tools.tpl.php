<h1 class="ui dividing header">
    Settings & Tools
</h1>
<form class="ui form" action="/admin/antivirus/settings" method="post">
<div class="ui segment">
    <h3 class="ui dividing header">
        Antivirus settings
    </h3>
    <div class="basic_width30 field">
        <label>Access Key</label>
        <input type="text" name="access_key" value="<?php echo $access_key ?>">
    </div>
    <p class="ui tiny c_red">This key is necessary to access to <a target="_blank" href="http://www.siteguarding.com">SiteGuarding API</a> features. Every website has uniq access key. Don't change it if you don't know what is it.</p>
</div>
<div class="ui segment">
    <h3 class="ui dividing header">
        General
    </h3>
    <div class="ui toggle checkbox <?php echo $registered; echo $membership; ?> inline field">
        <input type="checkbox" name="protected_by" <?php echo $protected_by; ?>>
        <label>Show 'Protected by'</label>
    </div>
    </br>
    <p class="ui tiny">
        <b>Server Time: </b><?php echo date("Y-m-d H:i:s"); ?>. This time stamp we will use in our logs. You can change timezone in <a href="/admin/config/regional/settings" target="_blank">Drupal General Settings</a>
    </p>
</div>
    <button type="submit" class="medium positive ui button">Save Settings</button>
</form>
<?php echo $footer; ?>


