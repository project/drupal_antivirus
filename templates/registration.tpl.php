<?php if($error != NULL)  { ?>
    <div class="ui red message">
        <?php if ($error == 'not_found') { ?>
        Domain is not exists or couldnt be resolved. Registration is available only for real domain. Please contact our support by email support@siteguarding.com
        <?php } elseif ($error == 'already_registered') { ?>
        Domain is already registered. Double registration is not permitted. The copy of your Access Key has been sent to your email. Go to Settings tab and put the key there. If you have any difficulties with activation your Access Key, please contact SiteGuarding.com support.
        <?php } if ($error == 'cant_communicate') { ?>
        Unfortunately, but your website cant communicate with siteguarding server. If you have any difficulties with registration the website, please contact SiteGuarding.com support.
        <?php }?>
    </div>
<?php } ?>
<div class="ui three column centered grid">
    <div class="row">
        <div class="center aligned column">
            <h1 class="ui icon header">
                <img class="ui image registration" src="<?php print "/" . drupal_get_path('module','drupal_antivirus') . "/images/reg_logo.svg"; ?>">
                <div class="content">
                Registration
                    <div class="sub header">Your website will be automatically registered on <a target="_blank" href="http://www.siteguarding.com">www.SiteGuarding.com</a>
                    </div>
                </div>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="center aligned column">
            <div class="ui padded segment">
                <h2 class="ui header">Already registered?
                    <div class="sub header">Go to Antivirus <a href="/admin/antivirus/settings">Settings</a> page and enter your Access Key.</div>
                </h2>
                <div class="ui horizontal divider">
                    Or
                </div>
                <form action="/admin/antivirus" method="post" class="ui large form registration" onsubmit="return form_confirm_registration(this);">
                    <div class="field">
                        <label><h2>Domain</h2></label>
                        <input type="text" name="sg_domain" value="<?php print $GLOBALS['base_url'] . "/"; ?>" readonly>
                    </div>
                    <div class="field">
                        <label><h2>Email</h2></label>
                        <input id="sg_email" type="text" name="sg_email">
                    </div>
                    <div class="field">
                        <div class="ui checkbox registration">
                            <input id="sg_term" type="checkbox" name="terms" tabindex="0">
                            <label><h4>I confirm to register my website on <a target="_blank" href="http://www.siteguarding.com">www.SiteGuarding.com</a></h4></label>
                        </div>
                    </div>
                    </br>
                    <button class="huge ui button registration" type="submit">
                        Confirm Registration
                    </button>
                </form>
            </div>
        </div>
    </div>
<div>