<h1 class="ui dividing header">
    Dashboard
</h1>
<div class="ui grid">
    <div class="row">
        <div class="six wide column">
            <div id="donutchart"></div>
        </div>
        <div class="ten wide column">
            <?php if (($membership == 'trial' || $membership == 'free') && (!isset($filemonitoring_plan) || $filemonitoring_plan == 'trial' || $filemonitoring_plan == 'free')) { ?>
                <h3 class="ui dividing header">
                    Premium Protection disabled
                </h3>
                <p>You have <span
                            class="ui red horizontal label"><?php echo $membership; ?></span>
                    limited version of Antivirus. Many options are not available for you. For example,
                    full
                    list of suspicious files, heuristic algorithm of files detection and much more.
                    Firewall
                    doesn't block suspicious requests to your website, therefore, your website is not
                    protected. Premium users get manual cleaning of the website, removal from the
                    blacklists, protection by more than 150 firewall rules and get free professional
                    consultation from the experts and security engineers. Upgrade to Premium today to
                    improve your protection.</p>
                <p>
                    <a href="https://www.siteguarding.com/en/protect-your-website" target="_blank"
                       class="ui medium positive button">Upgrade to PREMIUM</a>
                    <a href="https://www.siteguarding.com/en/website-antivirus" target="_blank"
                       class="ui medium primary basic button">Learn more</a>
                </p>
            <?php } elseif (($membership == 'pro') && (!isset($filemonitoring_plan) || $filemonitoring_plan == 'trial' || $filemonitoring_plan == 'free')) { ?>
                <h3 class="ui dividing header">
                    Premium Protection disabled
                </h3>
                <p>You have <span
                            class="ui green horizontal label"><?php echo $membership; ?></span>
                    version of Antivirus. List of suspicious files from the report is available for you,
                    and
                    if you know the programming languages you can clean it by your own. Premium users
                    get
                    manual cleaning of the website, removal from the blacklists, protection by more than
                    150
                    firewall rules and get free professional consultation from the experts and security
                    engineers. Upgrade to Premium today to improve your protection.</p>
                <p>
                    <a href="https://www.siteguarding.com/en/protect-your-website" target="_blank"
                       class="ui medium positive button">Upgrade to PREMIUM</a>
                    <a href="https://www.siteguarding.com/en/website-antivirus" target="_blank"
                       class="ui medium primary basic button">Learn more</a>
                </p>
            <?php } elseif ($membership == 'pro' && isset($filemonitoring_plan) && $filemonitoring_plan == 'basic') { ?>
                <h3 class="ui dividing header">
                    Premium Protection disabled
                </h3>
                <p>
                    You have <span
                            class="ui green horizontal label"><?php echo $membership; ?></span>
                    version of Antivirus. List of suspicious files from the report is available for you,
                    and
                    if you know the programming languages you can clean it by your own. Notice, that
                    basic
                    rules of the firewall are available for you, it blocks only part of the requests to
                    your
                    website, therefore, your website has low protection level. Premium users get manual
                    cleaning of the website, removal from the blacklists, protection by more than 150
                    firewall rules and get free professional consultation from the experts and security
                    engineers. Upgrade to Premium today to improve your protection.
                </p>
                <p>Expiration date: <span
                            class="ui blue horizontal basic label"><?php echo $filemonitoring_exp_date; ?></span>
                </p>
                <p>
                    <a href="https://www.siteguarding.com/en/protect-your-website" target="_blank"
                       class="ui medium positive button">Upgrade to PREMIUM</a>
                    <a href="https://www.siteguarding.com/en/website-antivirus" target="_blank"
                       class="ui medium primary basic button">Learn more</a>
                </p>
            <?php } elseif ($membership == 'pro' && isset($filemonitoring_plan) && $filemonitoring_plan == 'standard') { ?>
                <h3 class="ui dividing header">
                    Premium Protection disabled
                </h3>
                <p>
                    You have <span
                            class="ui green horizontal label"><?php echo $membership; ?></span>
                    version of Antivirus. List of suspicious files from the report, manual cleaning of
                    your
                    website, removal from the blacklists and much more are available for you. Notice,
                    that
                    standard rules of the firewall are available for you, it blocks only part of the
                    requests to your website, therefore, your website has medium protection level.
                    Premium
                    users are protected with additional 50 firewall rules, malware signatures and have
                    monitoring of the blacklists. Upgrade to Premium today to improve your protection.
                </p>
                <p>Expiration date: <span
                            class="ui blue horizontal basic label"><?php echo $filemonitoring_exp_date; ?></span>
                </p>
                <p>
                    <a href="https://www.siteguarding.com/en/protect-your-website" target="_blank"
                       class="ui medium positive button">Upgrade to PREMIUM</a>
                    <a href="https://www.siteguarding.com/en/website-antivirus" target="_blank"
                       class="ui medium primary basic button">Learn more</a>
                </p>
            <?php } elseif ($membership == 'pro' && $filemonitoring_plan == 'premium' || $filemonitoring_plan == 'business') { ?>
                <h3 class="ui dividing header">
                    Premium
                    Protection <?php if ($filemonitoring_exp_date > date("Y-m-d")) {
                        echo "enabled";
                    } else {
                        echo "disabled";
                    } ?>
                </h3>
                <p>
                    You have <span
                            class="ui green horizontal label"><?php echo $membership; ?></span>
                    version of Antivirus. Full list of the features is available for you. All firewall
                    rules
                    are available, it blocks all suspicious requests to your website, therefore, your
                    website has high protection level.
                </p>
                <p>Expiration date: <span
                            class="ui blue horizontal basic label"><?php echo $filemonitoring_exp_date; ?></span>
                </p>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="ui five steps">
            <div class="step">
                <i class="<?php if($membership == 'pro') {
                    echo "check green";
                } else {
                    echo "times red";
                } ?> icon"></i>
                <div class="content">
                    <div class="title">Antivirus</div>
                    <div class="description">Detect malware codes</div>
                </div>
            </div>
            <div class="step">
                <i class="<?php if (isset($filemonitoring_plan) && ($filemonitoring_plan == 'premium' || $filemonitoring_plan == 'business') && $filemonitoring_exp_date > date("Y-m-d")) {
                    echo "check green";
                } else {
                    echo "times red";
                } ?> icon"></i>
                <div class="content">
                    <div class="title">Firewall</div>
                    <div class="description">Realtime protection, block injections</div>
                </div>
            </div>
            <div class="step">
                <i class="<?php if (isset($filemonitoring_plan) && $filemonitoring_plan > date("Y-m-d")) {
                    echo "check green";
                } else {
                    echo "times red";
                } ?> icon"></i>
                <div class="content">
                    <div class="title">Monitoring 24/7</div>
                    <div class="description">Control all changes on the website</div>
                </div>
            </div>
            <div class="step">
                <i class="<?php if (isset($filemonitoring_plan) && ($filemonitoring_plan == 'premium' || $filemonitoring_plan == 'business') && $filemonitoring_exp_date > date("Y-m-d")) {
                    echo "check green";
                } else {
                    echo "times red";
                } ?> icon"></i>
                <div class="content">
                    <div class="title">Blacklist monitoring</div>
                    <div class="description">Control all blacklists</div>
                </div>
            </div>
            <div class="step">
                <i class="<?php if (isset($remote_backup_status) && $remote_backup_status == 1) {
                    echo "check green";
                } else {
                    echo "times red";
                } ?> icon"></i>
                <div class="content">
                    <div class="title">Website backup</div>
                    <div class="description">Full backup of the website</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="ten wide column">
            <div class="ui <?php if ($membership == 'pro') {
                echo "small green";
            } else {
                echo "large red";
            } ?> icon message">
                <i class="<?php if ($membership == 'pro') {
                    echo "check ";
                } else {
                    echo "times ";
                } ?> icon"></i>
                <div class="content">
                    <div class="header">
                        You have <?php echo ucfirst($membership) ?> version.
                    </div>
                    Available Scans: <?php echo $scans; ?> <br>
                    Valid till: <?php echo $exp_date; ?>
                </div>
            </div>
            <?php if (count($reports) > 0) {
                if ($last_scan_files_counters_main == 0 && $last_scan_files_counters_heuristic == 0) { ?>
                    <div class="ui green small icon message">
                        <i class="check icon"></i>

                        <div class="content">
                            <div class="header">
                                Website is clean
                            </div>
                            We didn't detect any problems with the files on your website.
                        </div>
                    </div>
                <?php } ?>
                <?php if ($last_scan_files_counters_main > 0) { ?>
                    <div class="ui red large icon message">
                        <i class="times icon"></i>

                        <div class="content">
                            <div class="header">
                                Website is infected
                            </div>
                            <?php if ($filemonitoring_status == 0) { ?>
                                <a class="mini ui blue button right floated" target="_blank"
                                   href="https://www.siteguarding.com/en/services/malware-removal-service">Clean
                                    Website</a>
                                We have detected virus / unsafe files on your website.
                            <?php } else { ?>
                                <a class="mini ui red button right floated" target="_blank" href="https://www.siteguarding.com/en/contacts">Send Request</a>
                                We have detected virus / unsafe files on your website. </br>
                                You have subscription with SiteGuarding.com and can request free cleaning. Please send us a ticket to request the cleaning service.
                            <?php } ?>
                        </div>
                    </div>
                <?php } else if ($last_scan_files_counters_heuristic > 0 ) { ?>
                    <div class="ui red large icon message">
                        <i class="times icon"></i>

                        <div class="content">
                            <div class="header">
                                Website has unsafe/infected files
                            </div>
                            <?php if ($filemonitoring_status == 0) { ?>
                                <a class="mini ui red button right floated" target="_blank"
                                   href="https://www.siteguarding.com/en/services/malware-removal-service">Clean
                                    Website</a>
                                We have detected virus / unsafe files on your website.
                            <?php } else { ?>
                                <a class="mini ui red button right floated" target="_blank"
                                   href="https://www.siteguarding.com/en/contacts">Send Request</a>
                                We have detected virus / unsafe files on your website.<br>
                                You have subscription with SiteGuarding.com and can request free cleaning. Please send us a ticket to request the cleaning service.
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="ui blue large icon message">
                    <i class="exclamation icon"></i>

                    <div class="content">
                        <div class="header">
                            Website never analyzed before
                        </div>
                        Please go to Antivirus section and scan your website.
                    </div>
                </div>
            <?php } ?>

            <?php if ($filemonitoring_status == 0) { ?>
                <div class="ui blue large icon message">
                    <i class="exclamation icon"></i>

                    <div class="content">
                        <div class="header">
                            Files Change Monitoring
                        </div>
                        <a class="mini ui blue button right floated" target="_blank"
                           href="https://www.siteguarding.com/en/protect-your-website">Protect your
                            website</a>
                        You don't have subscription for this service.<br>

                        <p class="ui mini">One of the services provided by us is the day-to-day scanning
                            and checking Your website for malware installation and changes in the
                            files.</p>

                        <p>If hacker upload any file, remove or inject malware codes into the website's
                            files. We will easy detect it and fix the issue.</p>
                    </div>
                </div>
            <?php } else { ?>
                <div class="ui green small icon message">
                    <i class="check icon"></i>

                    <div class="content">
                        <div class="header">
                            Files Change Monitoring
                        </div>
                        Your subscription is <?php echo $filemonitoring_plan; ?>
                        [ <?php echo $filemonitoring_exp_date; ?> ]
                    </div>
                </div>
            <?php } ?>

            <?php if ($firewall_status && isset($filemonitoring_exp_date) && $filemonitoring_exp_date > date("Y-m-d")) { ?>
                <div class="ui green small icon message">
                    <i class="check icon"></i>

                    <div class="content">
                        <div class="header">
                            Professional Website Firewall
                        </div>
                        Firewall is installed. A website firewall is an appliance, standalone plugin
                        that applies a set of rules to an HTTP conversation. Generally, these rules
                        cover common attacks such as cross-site scripting (XSS), backdoor requests and
                        SQL injection. By customizing the rules to your application, many attacks can be
                        identified and blocked.
                    </div>
                </div>
            <?php } else { ?>
                <div class="ui blue large icon message">
                    <i class="exclamation icon"></i>

                    <div class="content">
                        <div class="header">
                            Professional Website Firewall
                        </div>
                        <a class="mini ui blue button right floated" target="_blank"
                           href="https://www.siteguarding.com/en/protect-your-website">Protect your
                            website</a>
                        Firewall is not installed or not activated. We don't filter the traffic of your
                        website.<p class="mini">A website firewall is an appliance, standalone plugin
                            that applies a set of rules to an HTTP conversation. Generally, these rules
                            cover common attacks such as cross-site scripting (XSS), backdoor requests
                            and SQL injection. By customizing the rules to your application, many
                            attacks can be identified and blocked.
                    </div>
                </div>
            <?php } ?>

            <?php if ($filemonitoring_remote_backup_status == 0) { ?>
                <div class="ui blue large icon message">
                    <i class="exclamation icon"></i>

                    <div class="content">
                        <div class="header">
                            Website Backup service
                        </div>
                        <a class="mini ui blue button right floated" target="_blank"
                           href="https://www.siteguarding.com/en/importance-of-website-backup">Learn
                            More</a>
                        You don't have subscription for this service.<br>

                        <p class="mini">It is extremely important to have your website backed up
                            regularly. The website backup means that you can have a similar copy of your
                            content and data with you. You can keep it safe. Whatever happens to your
                            website, the data will be available to you, and you can use it later.</p>
                    </div>
                </div>
            <?php } else { ?>
                <div class="ui green small icon message">
                    <i class="check icon"></i>

                    <div class="content">
                        <div class="header">
                            Website Backup service
                        </div>
                        Backup service is activated.
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="six wide column">
            <div class="ui raised segment">
                <h3 class="ui dividing header">
                    Status
                </h3>
                <div><b>Version:</b> <?php if ($membership == 'pro') { ?>
                        <span
                                class="ui green horizontal label"><?php echo $membership; ?></span>
                    <?php } else { ?>
                        <span
                                class="ui red horizontal label"><?php echo $membership; ?></span>
                    <?php } ?>
                </div>
                <div>
                    <b>Valid till:</b> <?php echo $exp_date; ?>
                </div>
                <div>
                    <b>Available scans:</b> <?php echo $scans; ?>
                </div>
                <div>To get the latest status of your license, click <b>Refresh</b> button.</div>
                <div class="standard_alignment">
                    <form action="/admin/antivirus" method="post" class="ui form">
                        <input name="refresh" type="hidden" value="start">
                        <button class="medium positive ui button" type="submit">
                            Refresh
                        </button>
                        <a class="ui medium positive button" target="_blank"
                           href="https://www.siteguarding.com/en/buy-service/antivirus-complete-website-protection">Upgrade</a>
                        <a class="ui medium negative button" target="_blank"
                           href="https://www.siteguarding.com/en/services/malware-removal-service">Clean
                            Website</a>
                    </form>
                </div>
                <p>
                    <i class="user md green icon"></i>Premium customers can request free cleaning.
                    Please contact
                    <a href="https://www.siteguarding.com/en/contacts" target="_blank">SiteGuarding.com
                        support</a>
                </p>
            </div>
            <div class="ui raised segment">
                <h3 class="ui dividing header">
                    Latest Antivirus Reports
                </h3>
                <div class="ui list">
                    <?php $sum_reports = count($reports);
                    if ($sum_reports > 0) {
                        for ($i = 0; $i < $sum_reports; $i++) { ?>
                            <div class="item">
                                <i class="file alternate outline icon"></i>
                                <div class="content"><a
                                            href="<?php echo $reports[$i]['report_link']; ?>"
                                            target="_blank">Antivirus
                                        report <?php echo $reports[$i]['date']; ?></a></div>
                            </div>
                            <?php if ($i == 4) {
                                break;
                            }
                        }
                    } else { ?>
                        You don't have any reports. Go to Antivirus scanner tab and scan the website.
                    <?php } ?>
                </div>
                <div class="standard_alignment">
                    <b> Need help from professional web security expert? </b><br><br>
                    <a class="ui medium negative button" target="_blank"
                       href="https://www.siteguarding.com/en/services/malware-removal-service">Clean
                        Website</a>
                </div>
                <p>
                    <i class="user md green icon"></i>Premium customers can request free cleaning.
                    Please contact
                    <a href="https://www.siteguarding.com/en/contacts" target="_blank">SiteGuarding.com
                        support</a>
                </p>
            </div>
            <div class="ui raised segment">
                <h3 class="ui dividing header">
                    Login Attempts ( Administrator Users )
                </h3>
                <?php if(count($admin_users) > 0) { ?>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th>Username</th>
                        <th>Created</th>
                        <th>Access</th>
                        <th>Last login</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php foreach ($admin_users as $user) { ?>
                            <td><?php echo $user[0]; ?></td>
                            <td><?php echo date("Y-m-d",$user[1]); ?></td>
                            <td><?php echo date("Y-m-d",$user[2]); ?></td>
                            <td><?php echo date("Y-m-d",$user[3]); ?></td>
                            <td><?php if($user[4] == 1){echo 'Active';} else {echo 'Blocked';}; ?></td>
                        </tr>
                    <?php } } else { ?>
                        <p>You dont have users with administrator level</p>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>